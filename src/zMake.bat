@ECHO OFF

:RETURN

echo;
echo ************ アプリケーションメニュー *******************
echo [0]  : 終了
echo [1]  : HTML作成
echo [2]  : HELP作成
echo [3]  : doc 作成 (Word文書)
echo [4]  : pdf 作成 (テフ文書からpdf,docを生成)
echo [5]  : pdf 作成 (pdf 文書)rst2pdf★禁止
echo [6]  : epub作成 (電子ブック文書)
echo [7]  : コード管理リポジトリ
echo [8]  : Webサーバ開始
echo [80] : WWW-Browsing 
echo [9]  : 清掃
echo *********************************************************
echo;
set /p NUM= "起動させるアプリケーションを選択してください >"
echo;

rem カレントディレクトリ取得
cd /d %0\..

if "%NUM%"=="0" GOTO EXIT
if "%NUM%"=="1" GOTO HTML
if "%NUM%"=="2" GOTO HELP
if "%NUM%"=="3" GOTO DOC
if "%NUM%"=="4" GOTO LATEX2PDF
if "%NUM%"=="5" GOTO PDF
if "%NUM%"=="6" GOTO EPUB
if "%NUM%"=="7" GOTO HG
if "%NUM%"=="8" GOTO STARTWEBSVR
if "%NUM%"=="80" GOTO WWW-BROWSER
if "%NUM%"=="9" GOTO CLEAN

GOTO RETURN

rem ================================
:HTML
echo -- HTMLを生成 ---
echo;

call .\make.bat html
.\_build\html\index.html
del docx.log >NUL　2>&1

GOTO RETURN

rem ================================
:HELP
echo -- HELP_HTMLを生成 ---
del _build\htmlhelp\*.chm >NUL　2>&1
rem make htmlhelp
sphinx-build -b htmlhelp . _build\htmlhelp>> log_htmlhelp.log
python.exe _cv_hhp.py
python.exe _cv_hhx.py
python.exe _runhhc.pyw
GOTO EXIT

rem ================================
:DOC
echo -- ワード文書を生成 ---
del _build\docx\*.docx >NUL　2>&1
sphinx-build -b docx . _build\docx >> log_docx.log
cd _build\docx
cd .\_build\latex
for %%f in (*.docx) do "C:\Program Files\Windows NT\Accessories\wordpad.exe" %%f
GOTO EXIT

rem ================================
:PDF
echo -- pdfファイルを生成 ---
del _build\pdf\*.pdf >NUL　2>&1
rem  sphinx-build -b pdf . _build\pdf >> log_pdf.log
rem make pdf 
cd _build\pdf
for %%f in (*.pdf) do "C:\Program Files (x86)\Adobe\Acrobat 10.0\Acrobat\Acrobat.exe" %%f
GOTO EXIT

rem ================================
:EPUB
echo -- 電子ブックを生成 ---
make epub
PAUSE
GOTO EXIT

rem ================================
:HG
echo --- コード管理リポジトリ ---
start zhg.bat
GOTO EXIT

rem ================================
:STARTWEBSVR
del .\_build\html\cgi-bin\*.* /s /q
start zMakep.bat
echo --- Webサーバ開始 ---
XCOPY .\cgi-bin\*.* .\_build\html\cgi-bin\ /E /Y /D
XCOPY .\javascript\* .\_build\html\javascript\ /E /Y /D
cd _build\html
python -m CGIHTTPServer 8080

rem ================================
:WWW-BROWSER
start http://localhost:8080/index.html
GOTO EXIT

rem ================================
:LATEX2PDF
echo;
echo --- テフ文書texを生成し、テフtexからpdf、docを生成します ---
echo;
echo [連絡] 途中処理が中断し?が発生した場合は"?"を入力します。
echo [入力] "R"を押すと、エラーを無視して処理を続行します
echo;
echo;
set /p XX= "Enterを押してください。処理を開始します >>"
echo;

call .\make.bat clean
call make latex
cd .\_build\latex
platex -kanji=utf8 latex.tex
dvipdfmx latex.dvi
pandoc -s latex.tex -o latex.doc
call explorer .
REM acrobat.exe latex.pdf
GOTO EXIT

rem ================================
:CLEAN
echo -- ビルドファイル清掃 ---
call .\make.bat clean

GOTO RETURN

rem ================================
:EXIT
