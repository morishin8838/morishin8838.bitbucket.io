=========================
仕事の活動(Job Activity)
=========================
職歴
=========================
| 地元北海道で、高校教諭を目指す。教員試験に2度失敗し諦めました
| エンジニアを目指す。ベンチャー企業に就職。小さい会社なら開発に携われると考えた。しかし、甘かった。。当初、営業技術を担当。辛かったなぁ。。
| その後、転職をたくさんしました（涙）。好んで転職した訳ではなく、流れのままに生きてみました。。
| 優秀なエンジニアを目指した（つもりの）結果だと思う。自分の中で、優秀の定義がないだけかも。。

* 某精密メーカ 2006年～現在に至る
	* 次世代顕微鏡開発 2023年〜
		* 次世代病理用デジタル顕微鏡開発に携わる

	* Micorochip社 FPGA開発 2022年〜
		* Intel Cyclone-VからMicorochip PolarFireへFPGA置き換え開発に携わる

	* 顕微鏡用 エッジAI開発 2020年〜
		* 顕微鏡にエッジAIを組み込むための基礎的な技術開発を進めています
		* Xilinx社 FPGASoC(KriaSOM)を用いた電気制御システム設計を担当しています

	* 病理用デジタル顕微鏡開発 2019年〜
		* 電気ハードウエアの設計を担当しています

	* レーザ加工機ソフトウエア開発 2017年〜
		*  GUI software developper. mainly, in charge of front side.
		*  Qtを利用した画面設計、実装を担当

	* 次世代露光装置開発　真空制御部、静電チャック部ソフトウエア開発 2016年〜
		* Next Generation Exposure Equipment Developper. Vacum control and ESC.

	* 露光装置アプリケーションソフトウエア開発 2011年～
		* Software Architect and Functional Designer

	* 露光装置照明系 制御系開発  2009年～
		* Teamlead illumination Control team

	* 露光装置レンズ 制御系開発 2006年～
		* Embedded Hardware and Software Engineer

* ブリジストン 2005年～
	* タイヤ用スチールコード生産設備開発
		* 素晴らしい会社だったけど、退社。精密機械の開発に戻ることを決心。

* 某精密メーカの子会社 1998年～
	* 露光装置制御系開発（親会社へ出向）2000年～
		* Embedded Hardware and Software Engineer

	* 露光装置生産設備のメカ、電気設計 1998年～

* 株式会社 シム	1996年～
	* 外観検査機(SV-1800)メカ、電気設計

		* 記念パンフレット（当時、会社ホームページは無く、紙しか存在していないレア物）
		
		.. image:: pictures/20141018b.png

特許
=========================
	
	* astamuse（盛信一さん）
		http://astamuse.com/ja/patent/published/person/6841558

	* 2014年
		#. アプリケーションデータ共有システム（ボーグシステム）
			* 出願番号 2013-036891
			* 公開番号 2014-165423
			* http://astamuse.com/ja/published/JP/No/2014165423

	* 2011年
		#. 露光装置の投影レンズのヒータ配置の工夫
			* 特開2011-048126
			* http://pat.reserge.net/PatentDocument.php?an=2009196315&dbid=JPP

		#. ガラス面検出
			* 出願番号 2010-031672
			* 公開番号 2011-171386
			* http://astamuse.com/ja/published/JP/No/2011171386

		#. 液体ケーブル
			* 出願番号 2009-275707
			* 公開番号 2011-119476
			* http://astamuse.com/ja/published/JP/No/2011119476

	* 2010年
		#. 露光装置の投影レンズのAOM(音響光学変調素子)を用いた制御
			* 出願番号 2010-110150
			* 公開番号 2010-267966
			* http://astamuse.com/ja/published/JP/No/2010267966


そのほか
=========================

	* SPIE デジタルライブラリ 盛 信一(Shinichi Mori)
		https://www.spiedigitallibrary.org/profile/SHINICHI.MORI-1800

