==============================
お前だれよ？
==============================
| はじめまして、もりしん です。単身赴任生活になりましたが、コロナで自宅生活が多い（笑）
| 老眼が進行して、パソコン画面が本当に見えない（笑）

自己紹介(Introduction)
==============================

	:名前: 盛 信一 (Shinichi Mori)
	:出身: 北海道室蘭市です
	:住所: 栃木県宇都宮市在住 (Utsunomiya, Tochigi, Japan)
	:職業: 電気・ソフトウエア エンジニア (Engineer of electrical and software)
	:仕事: 某精密メーカに勤めています 

日記
==============================
	* [日記](http://nbviewer.jupyter.org/github/morishin8838/nbviewer/blob/master/001_Diary.ipynb)
	* [過去](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/001_Diary.ipynb)

ホームページ
==============================
	* [GitHub Jupyter-index](http://nbviewer.ipython.org/github/morishin8838/nbviewer/blob/master/index.ipynb)
	* [GitHub Jupyter-Top Page](http://nbviewer.ipython.org/github/morishin8838/)
	* [Bitbucke](https://bitbucket.org/morishin8838/nbviewer)

公開ホスティングサービス
==============================
	私がネット上に公開しているホスティングサービスの一覧です

	* https://bitbucket.org/morishin8838
		* プライベート中心でほとんど非公開

	* https://github.com/morishin8838
		* すべて公開。主に、notebook ipynbのコードを公開

	* https://hub.docker.com/u/morishin/
		* プライベートDocker Hubです
